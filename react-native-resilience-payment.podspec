Pod::Spec.new do |s|
  s.name             = 'react-native-resilience-payment'
  s.version          = '1.0.1'
  s.summary          = 'Yandex Payment component'

  s.description      = <<-DESC
    Yandex Payment component for react native
                       DESC

  s.homepage         = 'https://IArche@bitbucket.org/IArche/react-native-resilience-payment.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Resilience' => 'rresilience8@gmail.com' }
  s.source           = { :git => 'https://IArche@bitbucket.org/IArche/react-native-resilience-payment.git', :tag => s.version.to_s }
  s.ios.deployment_target = '10.0'
  s.swift_version = '4.2'
  s.source_files = 'ios/*'
  s.dependency 'React'
  s.dependency 'YandexCheckoutPayments'
end
