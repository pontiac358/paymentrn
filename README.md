
# react-native-resilience-map

## Установка

`$ npm install react-native-resilience-payment@git@bitbucket.org:IArche/react-native-resilience-payment.git --save`


#### iOS

1. Добавить в Podfile `source 'https://github.com/CocoaPods/Specs.git'`
2. Добавить в Podfile `pod 'react-native-resilience-payment', :path => '../node_modules/react-native-resilience-payment'`
3. Добавить в Podfile `pod 'YandexCheckoutPayments', :git => 'https://github.com/yandex-money/yandex-checkout-payments-swift.git', :tag => '3.2.0', :modular_headers => true`
4. Заменить `use_native_modules!` на `use_frameworks!`
5. Добавить TMXProfiling.framework и TMXProfilingConnections.framework в Frameworks проекта.
6. Выполнить команду `pod install`

#### Android

1. Добавить в app/build.gradle `implementation 'com.yandex.money:checkout:3.0.0'`

## Usage
```javascript
import RNRYPayment from 'react-native-resilience-payment';

var payment = new RNRYPayment();

payment.apiKey = "***";

// add payment.testMode = 1 for testing

const paymentToken = await payment.pay({
	"shop_name": "Shop Name",
	"amount": 1.0,
	"purchase_description": "Купи кипу пик, пик кину купи!",
	"payment_types": ["bank_card"]
});
console.warn(paymentToken.token);
console.warn(paymentToken.type);
